"""
Python3.8.3rc1
Naam programma: sorteer_getallen_efficiënt.py
Author: Ernest Lassman
laatst gewijzigs op 16/07/2020
Het programma sorteert de volledige lijst met de 
bubble-sortmethode na één passage.
"""
import time


def bubble_sort_1passage(lst):
    start = time.time()
    count = len(lst)  # Lengte van de lijst
    i, j = 0, 1
    while j < count:  # De volledige lijst nakijken
        if lst[i] > lst[j] and j != 0:  # Getallen op index i en j vergelijken
            # De getallen op index i en j veranderen van positie
            lst[i], lst[j] = lst[j], lst[i]
            # Vorige elementen in de lijst
            i -= 1
            j -= 1
        else:
            # Volgende elementen in de lijst
            i += 1
            j += 1
    stop = time.time()
    print(stop-start)
    return lst


if __name__ == "__main__":
    getallen = [107, 12, 8, 9, 100, 55, 34, 3, 2]
    sorteren = bubble_sort_1passage(getallen)
    print(sorteren)
